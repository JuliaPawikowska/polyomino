#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include "dodatki.h"
#include "Domino.h"

unsigned int Polyomino::total_blocks = 0, Polyomino::grid_height = 0, Polyomino::grid_width = 0, Polyomino::total_pieces = 0;;

Polyomino::Polyomino() {
	cols = rows = mirrored = rotated = 0;
	mark = ' ';
	piece = NULL;
}

Polyomino::Polyomino(const Polyomino& shape) {
	this->cols = shape.cols;
	this->rows = shape.rows;
	this->mark = shape.mark;
	this->rotated = 0;
	this->mirrored = 0;
	this->piece = new bool*[shape.rows];
	for (unsigned int i = 0; i < shape.rows; i++) this->piece[i] = new bool[shape.cols];
	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++) {
			if (shape.piece[i][j] == true) this->piece[i][j] = true;
			if (shape.piece[i][j] == false) this->piece[i][j] = false;
		}
}

//Wczytywanie z pliku
Polyomino* Polyomino::fLoad(const char* filename) {

	Polyomino *wczytuj = NULL;
	char linia[255];

	std::ifstream file(filename);

	if(!file) {
		std::cout << "Blad odczytu pliku!\n";
		return wczytuj;
	}

	file >> linia;
	if (strcmp(linia, "klocki") == 0) {
		file >> Polyomino::total_pieces;
	} else {
		std::cout << "Nie podano ilosci klockow! Uzupelnij plik wejsciowy!\n";
		file.close();
		return wczytuj;
	}

	file >> linia;
	if (strcmp(linia, "wys") == 0) {
		file >> Polyomino::grid_height;
	} else {
		std::cout << "Nie podano wysokosci planszy! Uzupelnij plik wejsciowy!\n";
		file.close();
		return wczytuj;
	}

	file >> linia;
	if (strcmp(linia, "szer") == 0) {
		file >> Polyomino::grid_width;
	} else {
		std::cout << "Nie podano szerokosci planszy! Uzupelnij plik wejsciowy!\n";
		file.close();
		return wczytuj;
	}

	file >> linia;
	if (strcmp(linia, "zbior") != 0) {
		std::cout << "Nie podano zbioru klockow! Uzupelnij plik wejsciowy!\n";
		file.close();
		return wczytuj;
	}

	wczytuj = new Polyomino[Polyomino::total_pieces];

	for (unsigned int i = 0; i < Polyomino::total_pieces; i++) {
		file >> linia;
		wczytuj[i].decode(linia);
		wczytuj[i].mark = linia[0];
	}

	file.close();

	return wczytuj;
}

//dekodowanie ciagu zer i jedynek na klocek
void Polyomino::decode(const char *code) {
	int wys = dodatki::countHeight(code);
	int szer = dodatki::countLength(code);

	bool **wynik = new bool*[wys];
	for(int i = 0; i < wys; i++) wynik[i] = new bool[szer];

	cols = szer;
	rows = wys;

	szer = wys = 0;

	for (unsigned int i = 1; i < strlen(code); i++){
		if (code[i] == ';') {
			szer = 0;
			wys++;
		} else {
			wynik[wys][szer] = code[i] != '0';
			wynik[wys][szer] == 1 ? Polyomino::total_blocks++ : 0;
			szer++;
		}

	}
	piece = wynik;
}

//Odbicie lustrzane klocka
void Polyomino::mirror(Polyomino& shape) {
	bool **temp = new bool*[shape.rows];
	for (unsigned int i = 0; i < shape.rows; i++) temp[i] = new bool[shape.cols];

	for (unsigned int i = 0; i < shape.rows; i++)
		for (unsigned int j = 0; j < shape.cols; j++){
			temp[i][j] = shape.piece[shape.rows - i - 1][j];
		}

	//przepisanie wartości
	for (unsigned int i = 0; i < shape.rows; i++)
		for (unsigned int j = 0; j < shape.cols; j++) {
			shape.piece[i][j] = temp[i][j];
		}

	//zwolnienie tablicy temp
	for (unsigned int i = 0; i < shape.rows; i++) delete [] temp[i];
	delete [] temp;

	shape.mirrored++;
}

//Obroty klockow
void Polyomino::rotate(Polyomino& shape) {
	bool **wynik = new bool*[shape.cols];
	for (unsigned int i = 0; i < shape.cols; i++) wynik[i] = new bool[shape.rows];

	for (unsigned int i = 0; i < shape.rows; i++)
		for (unsigned int j = 0; j < shape.cols; j++) {
			wynik[j][shape.rows - i - 1] = shape.piece[i][j];
		}

	for (int i = 0; i < shape.rows; i++) delete [] shape.piece[i];
	delete [] shape.piece;

	shape.piece = new bool*[shape.cols];
	for (int i = 0; i < shape.cols; i++) shape.piece[i] = new bool[shape.rows];

	for (int i = 0; i < shape.rows; i++)
		for (int j = 0; j < shape.cols; j++)
			shape.piece[j][i] = wynik[j][i];

	for (int i = 0; i < shape.cols; i++) delete [] wynik[i];
	delete [] wynik;

	//przypisanie nowych wartości
	int temp = shape.cols;
	shape.cols = shape.rows;
	shape.rows = temp;
	shape.rotated < 3 ? shape.rotated++ : shape.mirrored++;
}

Polyomino Polyomino::operator=(const Polyomino& shape) {
	cols = shape.cols;
	rows = shape.rows;
	mark = shape.mark;
	rotated = 0;
	mirrored = 0;
	piece = shape.piece;

	return *this;
}

std::ostream& operator<<(std::ostream& out, const Polyomino& shape) {
	for (unsigned int i = 0; i < shape.rows; i++)
		for (unsigned int j = 0; j < shape.cols; j++){
			if (j == 0 && i != 0) out << "\n";
			if (shape.piece[i][j] == 1) out << shape.mark; else out << ".";
		}

	return out;
}

Polyomino::~Polyomino() {
	for (unsigned int i = 0; i < rows; i++) delete [] piece[i];
	delete [] piece;
}
