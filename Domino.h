#include <iostream>

class Polyomino {
private:
	void decode(const char *code);

public:
	unsigned int cols, rows, mirrored, rotated;
	bool **piece;
	char mark;
	static unsigned int total_pieces, total_blocks, grid_height, grid_width;

	Polyomino();
	Polyomino(const Polyomino& shape);
	~Polyomino();
	static void rotate(Polyomino& shape);
	static void mirror(Polyomino& shape);
	static Polyomino* fLoad(const char* filename);
	Polyomino operator=(const Polyomino& shape);
	friend std::ostream& operator<<(std::ostream& out, const Polyomino& shape);
};
