all: dodatki.o Domino.o tester.o
	g++ -g -Wall tester.o Domino.o dodatki.o -o domino
	
dodatki.o: dodatki.cpp
	g++ -g -Wall dodatki.cpp -c -o dodatki.o
	
Domino.o: Domino.cpp
	g++ -g -Wall Domino.cpp -c -o Domino.o
	
tester.o: tester.cpp
	g++ -g -Wall tester.cpp -c -o tester.o

clean:
	rm -f *.o
