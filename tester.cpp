#include <iostream>
#include "Domino.h"

using namespace std;

typedef enum CONTROL {PIECE_UPLACEABLE = -2, NO_SOLUTION = -1, WORKING = 0, SOLUTION_F = 1, PIECE_PLACEABLE}CONTROL;

CONTROL solve(char **field, Polyomino* set, unsigned int count);

int main() {
	char file[255];

	cout << "Prosze podac nazwe pliku :\n";
	cin >> file;

	Polyomino *hem = Polyomino::fLoad(file);

	int plansza = (Polyomino::grid_height * Polyomino::grid_width);

	if (Polyomino::total_blocks > plansza) cout << "Plansza zbyt mala. Roznica : " << Polyomino::total_blocks - plansza << ". Prosze zmienic rozmiar w pliku wejsciowym i sprobowac ponownie.\n";
	else {
		char **field = new char*[Polyomino::grid_height];
		for (unsigned int i = 0; i < Polyomino::grid_height; i++) field[i] = new char[Polyomino::grid_width];

		for (unsigned int i = 0; i < Polyomino::grid_height; i++)
			for (unsigned int j = 0; j < Polyomino::grid_width; j++)
				field[i][j] = '.';

		 if ( solve(field, hem, Polyomino::total_pieces) == 1 ) {
			 for (unsigned int i = 0; i < Polyomino::grid_height; i++)
				 for (unsigned int j = 0; j < Polyomino::grid_width; j++) {
					 if (j == 0 && i != 0) cout << "\n";
					 cout << field[i][j];
				 }
		 } else cout << "Brak rozwiazania!\n";
	}
	cout << "\n";

	delete [] hem;
	return 0;
}


CONTROL solve(char **field, Polyomino* set, unsigned int count) {
	CONTROL control = WORKING;
	int f_x = 0, f_y = 0;

	if (count == 0) {
		return SOLUTION_F;
	}

	for (unsigned int main = 0; main < count; main++) {
		control = WORKING;
		while (control != NO_SOLUTION && control != SOLUTION_F) {
			for (unsigned int i = 0; i < Polyomino::grid_height; i++) { //znalezienie pierwszego wolnego miejsca
				for (unsigned int j = 0; j < Polyomino::grid_width; j++) {
					if (field[i][j] == '.') {
/*KOMENTY						cout << "Free node found at : i = " << i << " j = " << j << " \n";*/
						control = WORKING;
						while (control == WORKING) {
							control = PIECE_PLACEABLE;
								if ( (i + set[main].rows <= Polyomino::grid_height) && (j + set[main].cols <= Polyomino::grid_width) ) { //Pierwszy warunek zmieszczenia klocka
									for (unsigned int x = 0; x < set[main].rows; x++)
										for (unsigned int y = 0; y < set[main].cols; y++)
											if (set[main].piece[x][y] == 1 && field[i + x][j + y] != '.') control = WORKING; //drugi warunek zmieszczenia klocka
								} else control = WORKING;//Koniec if pierwszego warunku
							if (control == WORKING) { // rotacja/odbicie elementu
								if (set[main].rotated < 3) Polyomino::rotate(set[main]);
								else if (set[main].mirrored < 4) set[main].mirrored == 0 ? Polyomino::mirror(set[main]) : Polyomino::rotate(set[main]);
								else control = PIECE_UPLACEABLE;
							}
						} //Koniec while (WORKING)
					} //Koniec if field == '.'
					if (control == PIECE_PLACEABLE) {
						f_x = i;
						f_y = j;
						for (unsigned int x = 0; x < set[main].rows; x++)
							for (unsigned int y = 0; y < set[main].cols; y++)
								if (set[main].piece[x][y] == 1) field[f_x + x][f_y + y] = set[main].mark;

						Polyomino *set2 = new Polyomino[count - 1];
						for (unsigned int i = 0; i < count - 1; i++) set2[i] = set[i < main ? i : i + 1];

/*						cout << "PIECE " << set[main].mark << " PLACED, GOING DOWN\ndata --> count = " << count << " main = " << main << " f_x = " << f_x << " f_y = " << f_y << " rot = " << set[main].rotated << " mir = " << set[main].mirrored << "\n";*/

						control = solve(field, set2, count - 1);

/*						cout << "GOING UP data --> control = " << control << "\n";*/

						if (control == SOLUTION_F) return control;

						if (control == NO_SOLUTION) { //kolejna iteracja zawiodła, usunąć klocek, spróbować obrócić/odbić, ew. przejść do innego klocka startowego
							for (unsigned int x = 0; x < set[main].rows; x++)
								for (unsigned int y = 0; y < set[main].cols; y++)
									if (set[main].piece[x][y] == 1) field[f_x + x][f_y + y] = '.'; //klocek usunięty
							cout << "KLOCEK " << set[main].mark << " ZDJETO! Probuje w inny spospob...\n";
							if (set[main].rotated < 3) {
								Polyomino::rotate(set[main]);
								control = WORKING;
							} else if (set[main].mirrored < 4) {
								set[main].mirrored == 0 ? Polyomino::mirror(set[main]) : Polyomino::rotate(set[main]);
								control = WORKING;
							} else {
								control = PIECE_UPLACEABLE;
								set[main].rotated = set[main].mirrored = 0;
							}
						} //koniec if(NO_SOLUTION)
					} // koniec if(PIECE_PLACEABLE)
				} //Koniec for(j)
				if (control == PIECE_PLACEABLE) break;
			} // Koniec for(i)
			if (control == PIECE_UPLACEABLE) control = NO_SOLUTION; //Wyjście z while, przestawienie main
		} // Koniec while głównego
	} // Koniec for(main)

	return NO_SOLUTION;
}

